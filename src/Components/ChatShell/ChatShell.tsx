import React from 'react';

import { ChatTitle } from './ChatTitle/ChatTitle';
import { MessageList } from './MessageList/MessageList';
import { ChatForm } from './ChatForm/ChatForm';

import './Chat-Shell.css';




const ChatShell = () => {

    const fetchMessages = () => {
        this.setState({...ChatState, isFetching: true})
        fetch('https://api.npoint.io/a139a0497ad54efd301f')
          .then(response => response.json())
          .then(result => this.setState({messages: result, 
                                         isFetching: false}))
          .catch(e => console.log(e));
      }
    }
    



return (
    <div id="chat-container">
        <ChatTitle />
        <MessageList />
        <ChatForm />
    </div>
)
};

export { ChatShell };