import React from 'react';

import ChatState from '../ChatShell'
import './Chat-Title.css';

const ChatTitle = (props) => {
    return (
        <header>
            <div className="chat-name">My chat</div>
            <div className="participant-counter"></div>
            <div className="messages-counter"></div>
            <div className="last-message-date"></div>
        </header>
    );
}