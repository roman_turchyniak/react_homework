import React from 'react';

import './App.css';
import {Header} from './Components/Header/Header';
import {Footer} from './Components/Footer/Footer';
import { render } from '@testing-library/react';

const ChatState = {
  username: 'guest',
  avatar: "https://icon-library.com/images/incognito-icon/incognito-icon-22.jpg", 
  messages: []
}


class App extends React.Component{

  componentDidMount() {
    this.setState({...ChatState, isFetching: true})
    fetch('https://api.npoint.io/a139a0497ad54efd301f')
      .then(response => response.json())
      .then(result => this.setState({messages: result, 
                                     isFetching: false}))
      .then(()=>{{isFetching, ...this.state}=this.state;
                ChatState=(...this.state)}
      .catch(e => console.log(e));
  }
}

  return (
    <div>
      <Header></Header>
      
      <Footer></Footer>
    </div>
  );
}

export default App;
export 